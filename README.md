# Stochastic Rounding

Custom Pytorch Extension to Stochastically Round Tensors


To reproduce JoC experiments with stochastically rounded AMP:


Simply copy and paste the desired experiment’s command in ngc_commands.txt into a Pytorch19.06 ngc container with 8 32GB GPUS and 64 CPUS. For Resnet50 one also has to mount the imagenet-py dataset in ngc to /data/imagenet.